# redwarn-web
[![](https://data.jsdelivr.com/v1/package/npm/redwarn-web/badge)](https://www.jsdelivr.com/package/npm/redwarn-web)

The recent edits patrol and warning tool (aka RedWarn) is a JavaScript counter-vandalism tool, designed to be a user-friendly way to perform common moderation tasks.

## For full documentation, bugs, features and more info, see [WP:REDWARN](https://en.wikipedia.org/wiki/WP:REDWARN) on Wikipedia

You can help! If you find any bugs or would like new features, you can fix these or add them yourself. More technical documentation is coming in the user guide soon to help ease this process.
